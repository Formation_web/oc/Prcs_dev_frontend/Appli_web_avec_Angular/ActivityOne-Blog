import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Activity 1: Blog';

  posts = [
    {
      title: "Actualités du mois de Mai 2018",
      content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus sit vitae culpa doloremque laborum obcaecati voluptatem molestias ex eaque unde totam, veritatis tempore expedita dolore vel et error corporis minus.",
      loveIts: 0,
      created_at: new Date(),
    },
    {
      title: "Actualités du mois de Juin 2018",
      content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus sit vitae culpa doloremque laborum obcaecati voluptatem molestias ex eaque unde totam, veritatis tempore expedita dolore vel et error corporis minus.",
      loveIts: 0,
      created_at: new Date(),
    },
    {
      title: "Actualités du mois de Juillet 2018",
      content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Temporibus sit vitae culpa doloremque laborum obcaecati voluptatem molestias ex eaque unde totam, veritatis tempore expedita dolore vel et error corporis minus.",
      loveIts: 0,
      created_at: new Date()
    }
  ];

  constructor() {}

}
