export class PostService {

  listPosts = [
    {
      'title': 'Titre de mon post 1',
      'content': 'Contenu de mon post 1',
      'loveIt': 0,
      'created_at': new Date()
    },
    {
      'title': 'Titre de mon post 2',
      'content': 'Contenu de mon post 2',
      'loveIt': 0,
      'created_at': new Date()
    },
    {
      'title': 'Titre de mon post 3',
      'content': 'Contenu de mon post 3',
      'loveIt': 0,
      'created_at': new Date()
    }
  ];

}
